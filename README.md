# AndroWake

This project contains an Alarm Clock for Android, with the particularity that this Alarm Clock can read Csound .csd files. So, basically, you can wake up with algorithmic music,
or random base music etc... 

Currently, the alarm picker browser allows to check for csd files or audio files. It's planned to add a browser for res files designed for this purpose, so that there will be a 
base of ready to use alarms. 