package com.example.johann.androwake;

import android.app.Activity;
import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Intent;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.SeekBar;
import android.widget.Toast;

import com.csounds.CsoundObj;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.Calendar;

import csnd6.Csound;


public class WakeUpActivity extends AppCompatActivity {
    public static final int dayArray[]={Calendar.MONDAY,Calendar.TUESDAY,Calendar.WEDNESDAY,Calendar.THURSDAY,
            Calendar.FRIDAY,Calendar.SATURDAY,Calendar.SUNDAY};

    private Csound cs;
    private CsoundObj csoundObj;
    private MediaPlayer mediaPlayer;
    private String mode,path;
    private String filepath;
    private Button snooze,stopAlarm;
    private int alarmID,day,hour,minut;
    private Uri uri;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_wake_up);

        Log.d("Main","Starting activity");


        this.snooze=(Button) findViewById(R.id.snooze);
        this.stopAlarm=(Button) findViewById(R.id.stopAlarm);

        Intent intent=getIntent();
        //this.path=intent.getStringExtra("Path");
        this.mode=intent.getStringExtra("Mode");
        this.alarmID=intent.getIntExtra("alarmID",0);
        this.day=intent.getIntExtra("alarmDay",0);
        this.hour=intent.getIntExtra("alarmHour",0);
        this.minut=intent.getIntExtra("alarmMinut",0);
        this.uri=Uri.parse(intent.getStringExtra("URI"));


        Log.d("Main","day : " + day + " .. hour " + hour + " .. minut " + minut);


        this.filepath=Environment.getExternalStorageDirectory()+path;
        //Toast.makeText(getApplicationContext(),"Path : " + path + " Mode " + mode, Toast.LENGTH_LONG).show();

        this.setAudio(true);



        snooze.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setAudio(false);
                snoozeSchedule();
                finish();
            }
        });

        stopAlarm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setAudio(false);
                reSchedule();


                finish();
            }
        });


    }


    @Override
    protected void onDestroy() {
        // TODO Auto-generated method stub
        super.onDestroy();

    }



    public void setAudio(boolean on) {
        /*Vibrator vib=(Vibrator)getSystemService(getApplicationContext().VIBRATOR_SERVICE);
        long[] pattern={0,500,1000};
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            vib.vibrate(VibrationEffect.createOneShot(500, VibrationEffect.DEFAULT_AMPLITUDE));
        } else {
            //deprecated in API 26
            vib.vibrate(500);
        }*/


        if (on==true) {
            if (mode.equals("media")) {
                //Log.d("Main","PATH : " + path);
                //File file=new File(path);
                //Log.d("Main"," exists  : " +file.exists() + " can read : " + file.canRead());
                mediaPlayer=MediaPlayer.create(this,uri);
                mediaPlayer.setLooping(true);
                mediaPlayer.start();


            } else if (mode.equals("csound")) {
                //InputStream is=getContentResolver().openInputStream(uri);

                Toast.makeText(getApplicationContext(),"Csound will play",Toast.LENGTH_LONG).show();
                csoundObj=new CsoundObj(false,true);
                csoundObj.startCsound(createTempFile(getUriFileAsString(uri)));
            }

        } else {
            if (mode.equals("media")) {
                mediaPlayer.stop();
            } else if (mode.equals("csound")) {
                csoundObj.stop();
            }
        }
    }


    private void snoozeSchedule() {
        Calendar cal=Calendar.getInstance();
        cal.setLenient(true);
        cal.add(Calendar.MINUTE,9);
        Schedule(cal);
    }


    private void reSchedule() {
        Calendar cal=Calendar.getInstance();
        cal.setLenient(true);
        cal.set(Calendar.DAY_OF_WEEK,dayArray[day]);
        cal.set(Calendar.HOUR_OF_DAY,hour);
        cal.set(Calendar.MINUTE,minut);
        cal.set(Calendar.SECOND,0);

        Calendar now=Calendar.getInstance();

        if (cal.getTimeInMillis()<=now.getTimeInMillis()) {
            cal.add(Calendar.WEEK_OF_YEAR,1);
        }
        Schedule(cal);
    }


    private void Schedule(Calendar cal) {
        Intent intent=new Intent(getApplicationContext(),WakeUpActivity.class);
        //intent.putExtra("Path",path);
        intent.putExtra("Mode",mode);
        intent.putExtra("alarmID",alarmID);
        intent.putExtra("alarmDay",day);
        intent.putExtra("alarmHour",hour);
        intent.putExtra("alarmMinut",minut);
        intent.putExtra("URI",uri.toString());


        PendingIntent pi=PendingIntent.getActivity(getApplicationContext(),alarmID,intent,PendingIntent.FLAG_CANCEL_CURRENT);
        AlarmManager am=(AlarmManager)getSystemService(Activity.ALARM_SERVICE);
        //am.set(AlarmManager.RTC_WAKEUP,cal.getTimeInMillis(),pi);
        am.setAlarmClock(new AlarmManager.AlarmClockInfo(cal.getTimeInMillis(),pi),pi);
    }



    public void setSeekBarValue(SeekBar seekBar, double min, double max, double value) {
        double range = max - min;
        double percent = (value - min) / range;
        seekBar.setProgress((int)(percent * seekBar.getMax()));
    }



    protected String getResourceFileAsString(int resId) {
        StringBuilder str = new StringBuilder();

        InputStream is = getResources().openRawResource(resId);
        BufferedReader r = new BufferedReader(new InputStreamReader(is));
        String line;
        try {
            while ((line = r.readLine()) != null) {
                str.append(line).append("\n");
            }
        } catch (IOException ios) {

        }
        return str.toString();
    }



    protected String getUriFileAsString(Uri resId) {
        StringBuilder str = new StringBuilder();

        InputStream is = null;
        try {
            is = getContentResolver().openInputStream(resId);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        BufferedReader r = new BufferedReader(new InputStreamReader(is));
        String line;
        try {
            while ((line = r.readLine()) != null) {
                str.append(line).append("\n");
            }
        } catch (IOException ios) {

        }
        return str.toString();
    }



    protected File createTempFile(String csd) {
        File f = null;

        try {
            f = File.createTempFile("temp", ".csd", this.getCacheDir());
            FileOutputStream fos = new FileOutputStream(f);
            fos.write(csd.getBytes());
            fos.close();
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return f;
    }

}
