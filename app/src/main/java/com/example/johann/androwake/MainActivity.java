package com.example.johann.androwake;

import android.Manifest;
import android.app.Activity;
import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.res.Resources;
import android.database.Cursor;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.util.TypedValue;
import android.view.View;
import android.view.animation.AccelerateDecelerateInterpolator;
import android.widget.CompoundButton;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import com.example.johann.androwake.adapters.AlarmAdapter;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;



public class MainActivity extends AppCompatActivity implements OnCustomClickListener {


    private static final int REQUEST_CHOOSER=123;
    public static final int dayArray[]={Calendar.MONDAY,Calendar.TUESDAY,Calendar.WEDNESDAY,Calendar.THURSDAY,
            Calendar.FRIDAY,Calendar.SATURDAY,Calendar.SUNDAY};

    private DataBaseManager mydb;
    //variables
    private List<Alarm_Data> alarm_dataList;
    private ListView listView;
    private AlarmAdapter alarmAdapter;
    private int currentItem;
    private FloatingActionButton addAlarmBut;


/*
    // Used to load the 'native-lib' library on application startup.
    static {
        System.loadLibrary("native-lib");
    }
*/

    //constructor
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        mydb=new DataBaseManager(this);


        showData();
// Ask authorization for user file reading

        if (ContextCompat.checkSelfPermission(this,
                Manifest.permission.READ_EXTERNAL_STORAGE)
                != PackageManager.PERMISSION_GRANTED) {

            // Permission is not granted
            // Should we show an explanation?
            if (ActivityCompat.shouldShowRequestPermissionRationale(this,
                    Manifest.permission.READ_EXTERNAL_STORAGE)) {
                // Show an explanation to the user *asynchronously* -- don't block
                // this thread waiting for the user's response! After the user
                // sees the explanation, try again to request the permission.
            } else {
                // No explanation needed; request the permission
                ActivityCompat.requestPermissions(this,
                        new String[]{Manifest.permission.READ_EXTERNAL_STORAGE},REQUEST_CHOOSER);
                // MY_PERMISSIONS_REQUEST_READ_CONTACTS is an
                // app-defined int constant. The callback method gets the
                // result of the request.
            }
        } else {
            // Permission has already been granted
        }

        alarm_dataList = new ArrayList<>();

        StringBuffer data=showData();




        //alarm_dataList.add(new Alarm_Data(getApplicationContext(),alarm_dataList.size()));

        listView=(ListView)findViewById(R.id.list_alarm);
        alarmAdapter=new AlarmAdapter(this,alarm_dataList,this);
        listView.setAdapter(alarmAdapter);
        dataParse();
        //addData();

        this.addAlarmBut=(FloatingActionButton) findViewById(R.id.add_alarm);
        addAlarmBut.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alarm_dataList.add(new Alarm_Data(getApplicationContext(),alarm_dataList.size()));
                alarmAdapter.notifyDataSetChanged();
                addData();
                //Toast.makeText(getApplicationContext(),"HELLO THERE",Toast.LENGTH_LONG).show();
            }
        });

        //String thispath=Environment.getExternalStorageDirectory().getPath() + "/sdcard/Alarms/mod_high1.wav";

    }



    public void addData() {
        Alarm_Data alarmData=alarm_dataList.get(alarm_dataList.size()-1);
        boolean result=mydb.insertData(alarmData);
    }

    /**
     * A native method that is implemented by the 'native-lib' native library,
     * which is packaged with this application.
     */
//    public native String stringFromJNI();


    //Methods

    private void ResizeView(Alarm_Data currentAlarm,View customView) {

        float dip=94f;
        Resources r = getResources();
        float px=TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP,dip,r.getDisplayMetrics());
        Toast.makeText(getApplicationContext(), "MAIN ", Toast.LENGTH_SHORT).show();
        ListView.LayoutParams params=(ListView.LayoutParams) customView.getLayoutParams();

        if (!currentAlarm.isSelected()) {
            params.height=LinearLayout.LayoutParams.WRAP_CONTENT;
            currentAlarm.setSelected(true);

        }
        else {
            params.height=(int) px;
            currentAlarm.setSelected(false);
        }
            customView.setLayoutParams(params);
    }


    private void RotateButton(ImageButton imb) {
        float deg=imb.getRotation() + 180F;
        imb.animate().rotation(deg).setInterpolator(new AccelerateDecelerateInterpolator());
    }


    private void ScheduleAlarm(Alarm_Data currentAlarm, int day,boolean status) {

        if (status) {
            Calendar cal=Calendar.getInstance();
            cal.setLenient(true);
            cal.set(Calendar.DAY_OF_WEEK,dayArray[day]);
            cal.set(Calendar.HOUR_OF_DAY,currentAlarm.getHour());
            cal.set(Calendar.MINUTE,currentAlarm.getMinut());
            cal.set(Calendar.SECOND,0);

            Calendar now=Calendar.getInstance();

            if (cal.getTimeInMillis()<=now.getTimeInMillis()) {
                cal.add(Calendar.WEEK_OF_YEAR,1);
            }
            Log.d("Main","Could do it");

            int alarmID=currentAlarm.getAlarmID(day);
            Intent intent=new Intent(getApplicationContext(),WakeUpActivity.class);
            intent.putExtra("Path",currentAlarm.getAlarmPath());
            intent.putExtra("Mode",currentAlarm.getAlarmMode());
            intent.putExtra("alarmID",alarmID);
            intent.putExtra("alarmDay",day);
            intent.putExtra("alarmHour",currentAlarm.getHour());
            intent.putExtra("alarmMinut",currentAlarm.getMinut());
            intent.putExtra("URI",currentAlarm.getUriPath().toString());
            Log.d("Main","ID : "+alarmID + "  day : " +day +"  hourminut : " + currentAlarm.getHour() + " : " +  currentAlarm.getMinut());
            PendingIntent pi=PendingIntent.getActivity(getApplicationContext(),alarmID,intent,PendingIntent.FLAG_CANCEL_CURRENT);
            AlarmManager am=(AlarmManager)getSystemService(Activity.ALARM_SERVICE);

            am.setAlarmClock(new AlarmManager.AlarmClockInfo(cal.getTimeInMillis(),pi),pi);
            //am.set(AlarmManager.RTC_WAKEUP,cal.getTimeInMillis(),pi);
        } else {
            int alarmID=currentAlarm.getAlarmID(day);
            Intent intent=new Intent(getApplicationContext(),WakeUpActivity.class);
            PendingIntent pi=PendingIntent.getActivity(getApplicationContext(),alarmID,intent,PendingIntent.FLAG_CANCEL_CURRENT);
            AlarmManager am=(AlarmManager)getSystemService(Activity.ALARM_SERVICE);
            am.cancel(pi);
        }
    }






//Result of the file browse activity
    @Override
    protected void onActivityResult(int requestCode,int resultCode,Intent data) {
        super.onActivityResult(requestCode,resultCode,data);

        if (requestCode==REQUEST_CHOOSER && resultCode==RESULT_OK) {
            Uri selectedfile = data.getData();
            assert selectedfile != null;
            String uriPath=selectedfile.getPath();
            assert uriPath != null;
            String[] pathArray = uriPath.split(":");
            String fileName = pathArray[pathArray.length - 1];
            String realPath = Environment.getExternalStorageDirectory().getPath() + "/" + fileName;

            realPath=realPath.replaceAll("//","/");

            String alarmMode="";
            if(Pattern.matches(".*\\.csd",realPath)) {
                Toast.makeText(getApplicationContext(),"Csound File",Toast.LENGTH_LONG).show();
                alarmMode="csound";
            } else if (Pattern.matches(".*\\.wav|.*\\.mp3|.*\\.ogg|.*\\.flac",realPath)) {
                alarmMode="media";
            }

            Alarm_Data currentAlarmdata = alarm_dataList.get(currentItem);
            currentAlarmdata.setAlarmMode(alarmMode);
            //currentAlarmdata.setAlarmPath(realPath);
            currentAlarmdata.setUriPath(selectedfile);
            mydb.updateData(currentAlarmdata);
            Toast.makeText(getApplicationContext(),"Path : " + realPath
                    + "MODE  : " + alarmMode,Toast.LENGTH_LONG ).show();
        }
    }



    //Callbacks

    @Override
    public void OnCustomClickResize(View customView,int position,View parent) {
        Alarm_Data currentAlarm = (Alarm_Data) alarmAdapter.getItem(position);
        ResizeView(currentAlarm,parent);
        ImageButton imb=(ImageButton)customView.findViewById(R.id.show_hide);
        RotateButton(imb);

    }


    @Override
    public void OnCustomClickBrowse (View customView,int position,View parent) {
        Intent intent = new Intent()
                .setType("*/*")
                .setAction(Intent.ACTION_GET_CONTENT);
        currentItem=position;
        startActivityForResult(Intent.createChooser(intent, "Select a file"), REQUEST_CHOOSER);
//        Toast.makeText(getApplicationContext(),"Browser",Toast.LENGTH_LONG).show();
    }




    @Override
    public void OnCustomClickDelete(View customView,int position,View parent) {
        alarm_dataList.remove(position);
        alarmAdapter.notifyDataSetChanged();
        mydb.deleteData(alarm_dataList.get(position));
    }


    //callbacks toggle and switch
    @Override
    public void OnCheckedSwitch(CompoundButton customView, int position, View parent, boolean isChecked) {
        Alarm_Data currentAlarm = (Alarm_Data) alarmAdapter.getItem(position);
        Toast.makeText(getApplicationContext(),"State : " + (isChecked ? "on" : "off"),Toast.LENGTH_LONG).show();
        TextView textState=(TextView)parent.findViewById(R.id.switchState);

        if (isChecked){
            currentAlarm.setSwitchStat(true);
            textState.setText("ON");
            for (int i=0;i<7;i++) {
                if (currentAlarm.getDayState(i)) {
                    ScheduleAlarm(currentAlarm,i,true);
                }
            }


        } else {
            currentAlarm.setSwitchStat(false);
            textState.setText("OFF");
            for (int i=0;i<7;i++) {
                if (currentAlarm.getDayState(i)) {
                    ScheduleAlarm(currentAlarm,i,false);
                }
            }
//            currentAlarm.setAlarm(false);
        }
        mydb.updateData(currentAlarm);
    }

    @Override
    public void OnCheckedToggle(CompoundButton customView, int position, View parent, boolean isChecked,int idx) {
        Alarm_Data currentAlarm = (Alarm_Data) alarmAdapter.getItem(position);
        Toast.makeText(getApplicationContext(),"State : " + (isChecked ? "on" : "off"),Toast.LENGTH_LONG).show();
        if (isChecked){
            currentAlarm.setDayState(idx,true);
            customView.setBackgroundColor(R.color.colorPrimaryDark);

            if (currentAlarm.isSwitchStat()) {
                ScheduleAlarm(currentAlarm,idx,true);
            }

        } else {
            currentAlarm.setDayState(idx,false);
            customView.setBackgroundColor(Color.TRANSPARENT);

            if (currentAlarm.isSwitchStat()) {
                ScheduleAlarm(currentAlarm,idx,false);
            }


        }
        mydb.updateData(currentAlarm);

    }

    @Override
    public void OnCustomTimeChanged(TimePicker view, int hour, int minute, int position, View parent) {
        Alarm_Data currentAlarm = (Alarm_Data) alarmAdapter.getItem(position);
        currentAlarm.setClock(hour,minute);
        if (currentAlarm.isSwitchStat()) {
            for (int i=0;i<7;i++) {
                if (currentAlarm.getDayState(i)) {
                    ScheduleAlarm(currentAlarm,i,false);
                    ScheduleAlarm(currentAlarm,i,true);
                }
            }

        }
        mydb.updateData(currentAlarm);
    }




    //Data Methods

    public StringBuffer showData () {
        Cursor res=mydb.getAllData();
        if (res.getCount()==0) {
            Log.d("Main","Nothing found");
        }

        StringBuffer buffer = new StringBuffer();
        while (res.moveToNext()) {
            buffer.append("ID : " + res.getString(0) + "\n");
            buffer.append("SWITCH : " + res.getString(1)+ "\n");
            buffer.append("HOUR :" + res.getString(2)+ "\n");
            buffer.append("MINUT :" + res.getString(3)+ "\n");
            buffer.append("MODE :" + res.getString(4)+ "\n");
            buffer.append("URI :" + res.getString(5)+ "\n");
            buffer.append("DAYS :" + res.getString(6)+ "\n");
        }

        Log.d("Main","Data \n" +  buffer.toString());
        return buffer;
    }




    public void dataParse() {
        Cursor res=mydb.getAllData();
        if (res.getCount()==0) {
            Log.d("Main","Nothing found");
        }

        //StringBuffer buffer = new StringBuffer();
        while (res.moveToNext()) {
            int id=Integer.parseInt(res.getString(0));
            boolean switchStat=(Integer.parseInt(res.getString(1))!=0);
            int hour=Integer.parseInt(res.getString(2));
            int minuts=Integer.parseInt(res.getString(3));
            String mode=res.getString(4);
            String uri=res.getString(5);


            boolean[] days=new boolean[7];
            String dayStr=res.getString(6);


            for (int i=0;i<7;i++) {
                String cur=i+"_(.*)\n";
                Pattern p=Pattern.compile(cur);
                Matcher m=p.matcher(dayStr);
                if (m.find()) {
                    Log.d("Main", ("Day match : " + i + ".. " + m.group(1)));
                    days[i]=Boolean.parseBoolean(m.group(1));
                }
            }

            if (alarm_dataList.size()-1<id) {
                Alarm_Data currentAlarm=new Alarm_Data(getApplicationContext(),id);
                currentAlarm.setSwitchStat(switchStat);
                currentAlarm.setClock(hour,minuts);
                currentAlarm.setAlarmMode(mode);
                currentAlarm.setUriPath(Uri.parse(uri));
                for (int i=0;i<7;i++) {
                    currentAlarm.setDayState(i,days[i]);
                }
                alarm_dataList.add(currentAlarm);
            }


        }


    }

}
