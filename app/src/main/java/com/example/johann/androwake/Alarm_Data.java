package com.example.johann.androwake;

import android.content.Context;
import android.net.Uri;

import java.util.Arrays;
import java.util.Calendar;

public class Alarm_Data{



    //fields
        private boolean switchStat,selected;
        private boolean[] days;
        private int[] alarmID;

        private int[] clock;
        //For alarmMode and alarmPath, consider it could be mode = "csound" and path="res/raw/csdtest.csd"
        private String alarmMode, alarmPath;
        private Uri uriPath;

        private Context context;
        private int idx;
        //private Intent alarmintent;


    //constructor
    public Alarm_Data(Context ctxt,int idx) {
        this.idx=idx;

        this.switchStat=false;
        this.selected=false;
        this.clock=new int[2];
        Arrays.fill(clock,0);
        this.days=new boolean[7];
        Arrays.fill(days,Boolean.FALSE);

        this. alarmID=new int[7];
        setAlarmID(idx);

        this.context=ctxt;
        this.alarmMode="media";
        this.alarmPath="res/raw/low_obj3.wav";
        this.uriPath= Uri.parse("android.resource://" + context.getPackageName() + "/" + R.raw.low_obj3);
    }


    //methods

    public int getIdx() {return idx;}



    public void setSwitchStat(boolean switchStat) {
        this.switchStat=switchStat;
    }
    public boolean isSwitchStat() {return switchStat;}


    public void setDayState(int idx,boolean state) {
        days[idx]=state;
    }

    public boolean getDayState(int idx) {return days[idx];}


    public boolean isSelected() {
        return selected;
    }

    public void setSelected(boolean selected) {
        this.selected = selected;
    }


    public void setUriPath(Uri uri) {
        this.uriPath=uri;
    }

    public Uri getUriPath() {
        return uriPath;
    }

    public void setAlarmMode(String alarmMode) {
        this.alarmMode = alarmMode;
    }
    public void setAlarmPath(String alarmPath) {
        this.alarmPath=alarmPath;
    }

    public String getAlarmPath() {return alarmPath;}
    public String getAlarmMode() {return alarmMode;}


    public void setClock(int newHour, int newMinute) {
        clock[0]=newHour;
        clock[1]=newMinute;
    }

    public int getHour() {
        return clock[0];
    }

    public int getMinut() {
        return clock[1];
    }

    public void setAlarmID(int position) {
        for (int i=0;i<7;i++) {

            alarmID[i]=Integer.parseInt(Integer.toString(position+1) + Integer.toString(i+1));
        }
    }

    public int getAlarmID(int day) {
        return alarmID[day];
    }

}
