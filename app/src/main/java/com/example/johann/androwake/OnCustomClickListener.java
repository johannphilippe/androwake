package com.example.johann.androwake;

import android.view.View;
import android.widget.CompoundButton;
import android.widget.TimePicker;

public interface OnCustomClickListener {
    void OnCustomClickResize(View customView, int position,View parent);
    void OnCustomClickBrowse(View customView,int position,View parent);
    void OnCustomClickDelete(View customView,int position,View parent);

    void OnCheckedSwitch(CompoundButton buttonView, int position, View parent, boolean isChecked);
    void OnCheckedToggle(CompoundButton buttonView, int position, View parent, boolean isChecked,int idx);

    void OnCustomTimeChanged(TimePicker view, int hour, int minute, int position, View parent);

}
