package com.example.johann.androwake;


import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

public class DataBaseManager extends SQLiteOpenHelper {

    public static final String DATABASE_NAME="androwakedata.db";
    public static final String TABLE_NAME="waketab";
    public static final String COL_1 = "id";
    public static final String COL_2 = "switchstat";
    public static final String COL_3 = "hour";
    public static final String COL_4 = "minuts";
    public static final String COL_5 = "alarmmode";
    public static final String COL_6 = "uripath";
    public static final String COL_7 = "days";



    public static final int DATABASE_VERSION = 1;
    public static final String TableCreate="CREATE TABLE IF NOT EXISTS "+TABLE_NAME + " ("
            + COL_1 + " INTEGER,"
            + COL_2 + " INTEGER,"
            + COL_3 + " INTEGER,"
            + COL_4 + " INTEGER,"
            + COL_5 + " TEXT,"
            + COL_6 + " TEXT,"
            + COL_7 + " TEXT)";

    private Context context;

    public DataBaseManager(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
        Log.d("Main","Created yo");
        //SQLiteDatabase db = this.getWritableDatabase();
        this.context=context;
    }

    @Override
    public void onCreate(SQLiteDatabase db) {

        Log.d("Main",TableCreate);
        db.execSQL(TableCreate);
        //db.execSQL("CREATE TABLE " + TABLE_NAME + " (ID INTEGER PRIMARY KEY AUTOINCREMENT,SWITCH BOOLEAN,HOUR INTEGER,MINUT INTEGER) ");

    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_NAME);
        onCreate(db);
    }

    @Override
    public void onOpen(SQLiteDatabase db) {
        onCreate(db);
    }

    public boolean insertData(Alarm_Data alarmData) {
        SQLiteDatabase db=this.getWritableDatabase();
        ContentValues mContentValues=formatData(alarmData);
        //mContentValues.put(COL_1,1);
        long result=db.insert(TABLE_NAME,null,mContentValues);
        Log.d("Main","result = " +result);
        //db.close();

        //long result=-1;
        return result != -1;

    }



    public boolean updateData(Alarm_Data alarmData) {
        SQLiteDatabase db=this.getWritableDatabase();
        ContentValues contentValues=formatData(alarmData);
        db.update(TABLE_NAME,contentValues,"ID = ?",new String[] {Integer.toString(alarmData.getIdx())});
        return true;
    }




    public Integer deleteData(Alarm_Data alarmData) {
        SQLiteDatabase db=this.getWritableDatabase();
        return db.delete(TABLE_NAME,"ID = ?", new String[] {Integer.toString(alarmData.getIdx())});
    }


    public ContentValues formatData(Alarm_Data alarmData) {
        ContentValues mContentValues=new ContentValues();
        mContentValues.put(COL_1,alarmData.getIdx());
        mContentValues.put(COL_2,alarmData.isSwitchStat());
        mContentValues.put(COL_3,alarmData.getHour());
        mContentValues.put(COL_4,alarmData.getMinut());
        mContentValues.put(COL_5,alarmData.getAlarmMode());
        mContentValues.put(COL_6,alarmData.getUriPath().toString());


        String days="";
        for (int i=0;i<7;i++) {
            days+= i + "_"+ alarmData.getDayState(i) + "\n";
        }
        mContentValues.put(COL_7,days);

        return mContentValues;
    }

    public Cursor getAllData() {
        SQLiteDatabase db=this.getWritableDatabase();
        Cursor res=db.rawQuery("select * from " + TABLE_NAME,null);
        return res;
    }

}
