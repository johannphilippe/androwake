package com.example.johann.androwake.adapters;


import android.content.Context;
import android.graphics.Color;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.ToggleButton;

import com.example.johann.androwake.Alarm_Data;
import com.example.johann.androwake.CustomOnClickListener;
import com.example.johann.androwake.OnCustomClickListener;
import com.example.johann.androwake.R;

import java.util.List;



public class AlarmAdapter extends BaseAdapter {

    //fields
    private Context context;
    private List<Alarm_Data> mAlarm_dataList;
    private LayoutInflater inflater;
    private OnCustomClickListener callback;


    static class ViewHolder {
        ImageButton mImageButton;
        Switch mSwitch;
        ToggleButton[] days = new ToggleButton[7];
        Button alarmPicker,deleteAlarm;
        TextView selectedAlarm,isSwitched;
        TimePicker clock;

    }

    //constructor
    public AlarmAdapter(Context context, List<Alarm_Data> alarm_dataList, OnCustomClickListener callback) {
        this.context = context;
        this.mAlarm_dataList = alarm_dataList;
        this.inflater = LayoutInflater.from(context);
        this.callback = callback;

    }

    //methods
    @Override
    public int getCount() {
        return mAlarm_dataList.size();
    }

    @Override
    public Object getItem(int position) {
        return mAlarm_dataList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }


    @Override
    public View getView(int position, View convertView, final ViewGroup parent) {

        ViewHolder viewHolder = new ViewHolder();
        if (convertView == null) {
            convertView = inflater.inflate(R.layout.listmodel, parent, false);


            viewHolder.mImageButton = (ImageButton) convertView.findViewById(R.id.show_hide);
            viewHolder.mSwitch = (Switch) convertView.findViewById(R.id.switch_alarm);
            viewHolder.alarmPicker = (Button) convertView.findViewById(R.id.pickalarm);
            viewHolder.selectedAlarm = (TextView) convertView.findViewById(R.id.selected_alarm);
            viewHolder.deleteAlarm=(Button) convertView.findViewById(R.id.deleteAlarm);
            viewHolder.clock=(TimePicker) convertView.findViewById(R.id.timepick);
            viewHolder.isSwitched=(TextView)convertView.findViewById(R.id.switchState) ;


            Alarm_Data alarmData=mAlarm_dataList.get(position);

            viewHolder.clock.setHour(alarmData.getHour());
            viewHolder.clock.setMinute(alarmData.getMinut());
            viewHolder.clock.setIs24HourView(true);

            viewHolder.mSwitch.setChecked(alarmData.isSwitchStat());
            if (alarmData.isSwitchStat())
                viewHolder.isSwitched.setText("ON");
            else
                viewHolder.isSwitched.setText("OFF");

            for (int i = 0; i < 7; i++) {
                String curStr = "day_" + i;
                int resID = context.getResources().getIdentifier(curStr, "id", context.getPackageName());
                viewHolder.days[i] = (ToggleButton) convertView.findViewById(resID);
                viewHolder.days[i].setChecked(alarmData.getDayState(i));
                Log.d("Main","Day State  ; " + i + " .. " + alarmData.getDayState(i));
                if (alarmData.getDayState(i))
                    viewHolder.days[i].setBackgroundColor(R.color.colorPrimaryDark);
                else if (!alarmData.getDayState(i))
                    viewHolder.days[i].setBackgroundColor(Color.TRANSPARENT);
            }


            convertView.setOnClickListener(new CustomOnClickListener(callback, position, "resize", convertView, 0));
            viewHolder.mImageButton.setOnClickListener(new CustomOnClickListener(callback, position, "resize", convertView, 0));
            viewHolder.mSwitch.setOnCheckedChangeListener(new CustomOnClickListener(callback, position, "switch", convertView, 0));
            viewHolder.alarmPicker.setOnClickListener(new CustomOnClickListener(callback, position, "browse", convertView, 0));
            viewHolder.deleteAlarm.setOnClickListener(new CustomOnClickListener(callback,position,"delete",convertView,0));
            viewHolder.clock.setOnTimeChangedListener(new CustomOnClickListener(callback,position,"TimeChange",convertView,0));
            for (int i = 0; i < 7; i++) {
                viewHolder.days[i].setOnCheckedChangeListener(new CustomOnClickListener(callback, position, "dayToggle", convertView, i));
            }

            convertView.setTag(viewHolder);

            viewHolder.selectedAlarm.setText("Default");

        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }


        return convertView;
    }


}
