package com.example.johann.androwake;

import android.view.View;
import android.widget.CompoundButton;
import android.widget.Switch;
import android.widget.TimePicker;

public class CustomOnClickListener implements View.OnClickListener,Switch.OnCheckedChangeListener, TimePicker.OnTimeChangedListener {
    //fields
    private int position;
    private OnCustomClickListener callback;
    private String mode;
    private View parent;
    private int idx;


    //constructor
    public CustomOnClickListener(OnCustomClickListener callback,int pos,String mode,View parent,int idx) {
        //in the tuto he makes position=pos not this.position
        this.position=pos;
        this.callback=callback;
        this.mode=mode;
        this.parent=parent;
        this.idx=idx;
    }

    @Override
    public void onClick(View v) {
        if (mode=="resize") {
            callback.OnCustomClickResize(v,position,parent);
        } else if (mode=="browse") {
            callback.OnCustomClickBrowse(v,position,parent);
        } else if (mode=="delete") {
            callback.OnCustomClickDelete(v,position,parent);
        }
    }


    @Override
    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
        if (mode=="switch") {
            callback.OnCheckedSwitch(buttonView,position,parent,isChecked);
        } else if (mode=="dayToggle") {
            callback.OnCheckedToggle(buttonView,position,parent,isChecked,idx);
        }
    }


    @Override
    public void onTimeChanged(TimePicker view, int hourOfDay, int minute) {
        callback.OnCustomTimeChanged(view,hourOfDay,minute,position,parent);
    }
}
